**main.prg
**Date: 1/27/2020
**Description: Simple REST Integration Application
**Author: Eric Mican

**setup application
_screen.WindowState = 0
_screen.Caption = "Simple REST Integration Application"

SET PATH TO "C:\Git\TwoFoxPro"
SET PROCEDURE TO c:\Git\twofoxpro\jsonbuilder.prg additive

** every time the application is run from main.prg, the collection will reset.  
public gcJira, gcConf
gcJira = CREATEOBJECT("collection")
gcConf = CREATEOBJECT("collection")

oIssue = CREATEOBJECT("empty")
oConfRcd = Createobject("Empty")

** init with blank record to !prevent invalid property error when launching form
ADDPROPERTY(oIssue, "id", "")
ADDPROPERTY(oIssue, "key", "")
ADDPROPERTY(oIssue, "self", "")
ADDPROPERTY(oIssue, "summary", "")
gcJira.add(oIssue, "1")


AddProperty(oConfRcd, "id", "")
AddProperty(oConfRcd, "space", "")
AddProperty(oConfRcd, "self", "" )
AddProperty(oConfRcd, "summary", "")
gcConf.Add(oConfRcd, "1")

DO FORM frmmainview

PROCEDURE procQuitNow
  ON SHUTDOWN  &&removes shutdown trap and prevents looping
  QUIT &&terminates the exe
ENDPROC

ON KEY LABEL ALT+F4 DO procQuitNow
ON SHUTDOWN DO procQuitNow