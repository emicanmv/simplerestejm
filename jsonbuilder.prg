**********************************
*
* ASI Atlassian REST API Library
*
* Author: Neal Adams
* Date: 5/29/2014
*
**********************************

&& Base class for a json string.
define class jsonbuilder as custom

	tag = 'BUILD'
	xcTitle = 'BASE'
	xcBaseString = ''
	nCount = 0
	
	&& Adds an element that supports a multiple element value
	&& Example: "space":{"key":"MDEV"}
	procedure addBuilder
	parameters m.xcTitle
	local m.xcName
	
		this.nCount = this.nCount + 1
		m.xcName = 'OBJ' + alltrim(str(this.nCount))
		this.AddObject(m.xcName, 'jsonbuilder')
		this.&xcName .xcTitle = m.xcTitle
		
		return m.xcName
	
	endproc
	
	&& Adds an element that supports a single element value
	&& Example: "key":"MDEV"
	procedure addValue
	parameters m.xcTitle, m.xcValue, m.lIsNumber
	local m.xcName
	
		this.nCount = this.nCount + 1
		m.xcName = 'OBJ' + alltrim(str(this.nCount))
		this.AddObject(m.xcName, 'jsonvalue')
		this.&xcName .xcType = iif(empty(m.lIsNumber), 'C', 'N')
		this.&xcName .xcTitle = m.xcTitle
		this.&xcName .xcValue = m.xcValue
		
		return m.xcName
	
	endproc
	
	&& Adds an element that supports an array of multiple element values
	&& Example: "space":[{"key":"MDEV"},{"key":"ENG"}]
	procedure addArray
	parameters m.xcTitle
	local m.xcName
	
		this.nCount = this.nCount + 1
		m.xcName = 'ARR' + alltrim(str(this.nCount))
		this.AddObject(m.xcName, 'jsonarray')
		this.&xcName .xcTitle = m.xcTitle
	
		return m.xcName
	
	endproc
	
	&& Creates elements based on a given string
	procedure createFromString
	parameters m.xcString
	local m.nCt, m.cChar, m.cLastChr, m.xcTitle, m.xcValue, m.lColon, m.lLastEscape, m.lIsNumber
	local m.lRecordTitle, m.lRecordValue, m.lFinishedTitle, m.oObj, m.xcInnerString, m.nTotal
	
		this.clear()
		
		&& Cleanup string
		m.xcString = alltrim(strtran(m.xcString, chr(13)+chr(10)))
		
		** For some reason confluence adds some crap-ass brackets into their shitty return
		*m.xcString = alltrim(strtran(m.xcString, '[{', '{'))
		*m.xcString = alltrim(strtran(m.xcString, '}]', '}'))
		
		
		m.lRecordTitle = .F.
		m.lRecordValue = .F.
		m.lColon = .F.
		m.lLastEscape = .F.
		m.lFinishedTitle = .F.
		m.xcTitle = ''
		m.xcValue = ''
		m.lIsNumber = .F.
		m.nTotal = len(m.xcString)
		
		
		for m.nCt = 1 to m.nTotal
			if m.nCt > 1
				m.cLastChar = m.cChar
			endif
			m.cChar = substr(m.xcString, m.nCt, 1)
		
			do case
				case m.lRecordTitle and m.cChar <> '"'
					m.xcTitle = m.xcTitle + m.cChar
				case m.lRecordValue and m.cChar <> '"' and m.cChar <> ',' and (m.cChar <> '}' or (m.cChar == '}' and !m.lIsNumber))
					m.xcValue = m.xcValue + m.cChar
				&& Value can be an empty string - catch that case here
				case m.lColon and m.cChar = '"' and m.cLastChar = '"' and empty(m.xcValue)
					m.xcValue = 'EMPTY'
			endcase
		
			&& Skip escape character
			if m.cChar = '\' or m.lLastEscape
				if m.cChar = '\'
					m.lLastEscape = .T.
				else
					if m.lRecordValue
						m.xcValue = m.xcValue + m.cChar
					endif
					m.lLastEscape = .F.
				endif
				loop
			endif
			
			&& We have completed the title.  Now determine what kind of object to add
			if m.lFinishedTitle and !m.lRecordValue
				do case
					case m.cChar = '['
						m.oObj = this.addArray(m.xcTitle)
						m.xcInnerArray = this.getInnerArray(substr(m.xcString,m.nCt))
						this.&oObj .CreateArrayFromString(m.xcInnerArray)
						m.xcString = strtran(m.xcString, m.xcInnerArray)
						m.xcTitle = ''
						m.xcValue = ''
						m.lColon = .F.
						m.lFinishedTitle = .F.
					case m.cChar = '{'
						m.oObj = this.addBuilder(m.xcTitle)
						m.xcInnerString = this.getInnerString(substr(m.xcString,m.nCt))
						this.&oObj .CreateFromString(m.xcInnerString)
						m.xcString = strtran(m.xcString, m.xcInnerString)
						m.xcTitle = ''
						m.xcValue = ''
						m.lColon = .F.
						m.lFinishedTitle = .F.
				endcase
			endif
			
			&& Catch the case where the value is not a string and is not surrounded by quotes
			if m.lColon and m.cChar <> '"' and m.cChar <> ',' and m.cChar <> '}' and !m.lRecordValue
				m.xcValue = m.xcValue + m.cChar
				m.lIsNumber = .T.
				m.lRecordValue = .T.
			endif
			
			if m.cChar = ':'
				m.lColon = .T.
				m.lFinishedTitle = .T.
			endif
			
			if m.cChar = ',' or m.cChar = '}'
				&& A comma or an end bracket does not necessarily signal the end of a pair
				&& Commas and brackets can be used in the value, so if its not a number and RecordValue is still on, keep chugging
				if !m.lIsNumber and m.lRecordValue
					m.xcValue = m.xcValue + m.cChar
				else
					if !empty(m.xcValue)
						this.addValue(m.xcTitle, iif(m.xcValue = 'EMPTY', '', m.xcValue), m.lIsNumber)
					endif
					m.lColon = .F.
					m.lFinishedTitle = .F.
					m.lRecordValue = .F.
					m.lIsNumber = .F.
					m.xcTitle = ''
					m.xcValue = ''
				endif
			endif
			
			if m.cChar = '"'
			
				do case
					case empty(m.xcTitle)
						m.lRecordTitle = .T.
					case !m.lColon
						m.lRecordTitle = .F.
					case empty(m.xcValue)
						m.lRecordValue = .T.
					case m.lColon
						m.lRecordValue = .F.
				endcase
			endif
		
		endfor		
	
	endproc
	
	&& Gets the inner json string.  String passed must start with beginning {
	procedure getInnerString
	parameters m.xcString
	local m.xcReturn, m.nCt, m.nBrak, m.cChar
	
		m.nBrak = 0
		m.xcReturn = ''
		for m.nCt = 1 to len(m.xcString)
		
			m.cChar = substr(m.xcString, m.nCt, 1)
			do case
				case m.cChar = '{'
					m.nBrak = m.nBrak + 1
				case m.cChar = '}'
					m.nBrak = m.nBrak - 1
			endcase
			m.xcReturn = m.xcReturn + m.cChar
			
			if m.nBrak = 0
				exit
			endif
		
		endfor
		
		return m.xcReturn
	
	endproc
	
	procedure getInnerArray
	parameters m.xcString
	local m.xcReturn, m.nCt, m.nBrak, m.cChar
		m.nBrak = 0
		m.xcReturn = ''
		for m.nCt = 1 to len(m.xcString)
		
			m.cChar = substr(m.xcString, m.nCt, 1)
			do case
				case m.cChar = '['
					m.nBrak = m.nBrak + 1
				case m.cChar = ']'
					m.nBrak = m.nBrak - 1
			endcase
			m.xcReturn = m.xcReturn + m.cChar
			
			if m.nBrak = 0
				exit
			endif
		
		endfor
		
		return m.xcReturn
			
	endproc
	
	&& Creates a string based on elements
	procedure createString
	local m.xcString
	
		m.xcString = '{'
	
		for each m.oElem in this.objects
			m.xcString = m.xcString + this.quotes(m.oElem.xcTitle) + ':'
			if m.oElem.tag = 'BUILD'
				m.xcString = m.xcString + m.oElem.createString() + ','
			else
				m.xcString = m.xcString + iif(m.oElem.xcType = 'C', this.quotes(m.oElem.xcValue), m.oElem.xcValue) + ','
			endif
		endfor
		
		m.xcString = m.xcString + '}'		
		m.xcString = strtran(m.xcString, ',}', '}')
		
		return m.xcString
	
	endproc
	
	procedure quotes
	parameters m.xcVal
	
		return '"' + alltrim(m.xcVal) + '"'
	
	endproc
	
	&& Get the value of the requested object
	&& Make request '.' delimited for nested requests
	procedure getValue
	parameters m.xcRequest
	local m.xcReq, m.oObj, m.xcResult
	
		m.xcResult = ''
	
		if '.' $ m.xcRequest
			m.xcReq = substr(m.xcRequest, 1, at('.', m.xcRequest, 1) - 1)
			m.xcRequest = substr(m.xcRequest, at('.', m.xcRequest, 1) + 1)
		else	
			m.xcReq = m.xcRequest
			m.xcRequest = ''
		endif
		
		for each m.oObj in this.Objects
			if upper(m.oObj.xcTitle) = upper(m.xcReq)
				if !empty(m.xcRequest) and (m.oObj.tag = 'BUILD' or m.oObj.tag = 'ELEM')
					m.xcResult = m.oObj.getValue(m.xcRequest)
				else
					if m.oObj.tag = 'VALUE'
						m.xcResult = m.oObj.xcValue
					else
						m.xcResult = m.oObj.createString()
					endif
				endif
				exit
			endif
		endfor
		
		release m.oObj
		
		return m.xcResult
	
	endproc

	&& Set the value of the requested object
	&& Make request '.' delimited for nested requests
	procedure setValue
	parameters m.xcRequest, m.xcValue
	local m.xcReq, m.oObj, m.xcResult
	
		if '.' $ m.xcRequest
			m.xcReq = substr(m.xcRequest, 1, at('.', m.xcRequest, 1) - 1)
			m.xcRequest = substr(m.xcRequest, at('.', m.xcRequest, 1) + 1)
		else	
			m.xcReq = m.xcRequest
			m.xcRequest = ''
		endif
		
		for each m.oObj in this.Objects
			if upper(m.oObj.xcTitle) = upper(m.xcReq)
				if !empty(m.xcRequest) and m.oObj.tag = 'BUILD'
					m.oObj.setValue(m.xcRequest, m.xcValue)
				else
					if m.oObj.tag = 'VALUE'
						m.oObj.xcValue = m.xcValue
					else
						m.oObj.clear()
						m.oObj.createFromString(m.xcValue)
					endif
				endif
				exit
			endif
		endfor
		
		release m.oObj
			
	endproc
	
	&& Clears all builders and values from the builder
	procedure clear
	
		do while type('this.Objects[1]') = 'O'
			this.RemoveObject(this.Objects[1].name)
		enddo
	
	endproc

enddefine

&& Class used by jsonbuilder
define class jsonvalue as custom

	tag = 'VALUE'
	xcTitle = ''
	xcValue = ''
	
	&& Possible Types:
	&& 'C' - String (default)
	&& 'N' - Number
	&& 'A' - Array
	&& 'L' - Boolean (true, false, null)
	xcType = 'C'

enddefine

define class jsonarray as jsonbuilder

	tag = 'ARRAY'
	nCount = 0
	m.xcTitle = ''
	
	procedure CreateArrayFromString
	parameters m.xcString
	local m.xcInnerString
	
		** Get rid of inital and ending square brackets
		m.xcString = substr(m.xcString, 2, len(m.xcString) -2)
	
		do while '{' $ m.xcString
			m.oObj = this.addBuilder()
			m.xcInnerString = this.getInnerString(m.xcString)
			this.&oObj .CreateFromString(m.xcInnerString)
			m.xcString = substr(m.xcString, len(m.xcInnerString) + 2)
		enddo
	
	endproc
	
	&& Adds an element that supports a multiple element value
	&& Example: "1":{"key":"MDEV"}
	procedure addBuilder
	local m.xcName
	
		this.nCount = this.nCount + 1
		m.xcName = 'ELEM' + alltrim(str(this.nCount))
		this.AddObject(m.xcName, 'jsonbuilder')
		this.Tag = 'ELEM'
		this.&xcName .xcTitle = alltrim(str(this.nCount))
		
		return m.xcName
	
	endproc
	
enddefine
